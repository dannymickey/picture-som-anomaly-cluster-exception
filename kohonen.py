import numpy as np
import cv2
import math
import copy
import tensorflow as tf


VEC_LEN = 3
DECAY_RATE = 0.96 # About 100 iterations.
MIN_ALPHA = 0.01
RADIUS_REDUCTION_POINT = 0.023  # Last 20% of iterations.
# G B R
FULL_WEIGHT_VECTOR = {
                        '1': [0.0, 0.0, 0.0], #
                        '2': [0.0, 0.0, 0.5], #
                        '3': [0.0, 0.0, 1.0], #
                        '4': [0.0, 0.5, 0.0], #
                        '5': [0.0, 0.5, 0.5], #
                        '6': [0.0, 0.5, 1.0], #
                        '7': [0.0, 1.0, 0.0], #
                        '8': [0.0, 1.0, 0.5], #
                        '9': [0.0, 1.0, 1.0], #
                        '10': [0.5, 0.0, 0.0], #
                        '11': [0.5, 0.0, 0.5], #
                        '12': [0.5, 0.0, 1.0], #
                        '13': [0.5, 0.5, 0.0], #
                        '14': [0.5, 0.5, 0.5], #
                        '15': [0.5, 0.5, 1.0], #
                        '16': [0.5, 1.0, 0.0], #
                        '17': [0.5, 1.0, 0.5], #
                        '18': [0.5, 1.0, 1.0], #
                        '19': [1.0, 0.0, 0.0], #
                        '20': [1.0, 0.0, 0.5], #
                        '21': [1.0, 0.0, 1.0], #
                        '22': [1.0, 0.5, 0.0], #
                        '23': [1.0, 0.5, 0.5], #
                        '24': [1.0, 0.5, 1.0], #
                        '25': [1.0, 1.0, 0.0], #
                        '26': [1.0, 1.0, 0.5], #
                        '27': [1.0, 1.0, 1.0], #


                        }

class Kohonen:
    def __init__(self, vectorLength, maxClusters, numPatterns, minimumAlpha, decayRate, reductionPoint, weightArray, picture, names):
        self.mVectorLen = vectorLength
        self.mMaxClusters = maxClusters
        self.mNumPatterns = numPatterns
        self.mMinAlpha = minimumAlpha
        self.mDecayRate = decayRate
        self.mReductionPoint = reductionPoint
        self.mAlpha = 0.6
        self.names = names
        self.d = []
        self.w = weightArray
        self.picture = picture
        return

    def compute_input(self, vectorArray, vectorNumber):
        self.d = [0.0] * self.mMaxClusters
        for i in range(self.mMaxClusters):
            for j in range(self.mVectorLen):
                self.d[i] += math.pow((self.w[i][j] - vectorArray[vectorNumber][j]), 2)
        return
    def get_minimum(self, nodeArray):
        minimum = 0
        foundNewMinimum = False
        done = False
        while not done:
            foundNewMinimum = False
            for i in range(self.mMaxClusters):
                if i != minimum:
                    if nodeArray[i] < nodeArray[minimum]:
                        minimum = i
                        foundNewMinimum = True
            if foundNewMinimum == False:
                done = True
        return minimum
    def update_weights(self, vectorNumber, dMin, patternArray):
        for i in range(self.mVectorLen):
            self.w[dMin][i] = self.w[dMin][i] + (self.mAlpha * (patternArray[vectorNumber][i] - self.w[dMin][i])) # WINNER
            if self.mAlpha > self.mReductionPoint:
                if (dMin > 0) and (dMin < (self.mMaxClusters - 1)):
                    self.w[dMin - 1][i] = self.w[dMin - 1][i] + (self.mAlpha * (patternArray[vectorNumber][i] - self.w[dMin - 1][i]))
                    self.w[dMin + 1][i] = self.w[dMin + 1][i] + (self.mAlpha * (patternArray[vectorNumber][i] - self.w[dMin + 1][i]))
                else:
                    if dMin == 0:
                        self.w[dMin + 1][i] = self.w[dMin + 1][i] + (self.mAlpha * (patternArray[vectorNumber][i] - self.w[dMin + 1][i]))
                    else:
                        self.w[dMin - 1][i] = self.w[dMin - 1][i] + (self.mAlpha * (patternArray[vectorNumber][i] - self.w[dMin - 1][i]))
        return
    def training(self, patternArray):
            # print(patternArray)
        iterations = 0
        reductionFlag = False
        reductionPoint = 0
        while self.mAlpha > self.mMinAlpha:
            iterations += 1
            print("Epoch: " + str(iterations), end="\r")
            for i in range(self.mNumPatterns):
                self.compute_input(patternArray, i)
                dMin = self.get_minimum(self.d)
                self.update_weights(i, dMin, patternArray)

            # Reduce the learning rate.
            self.mAlpha = self.mDecayRate * self.mAlpha
            # Reduce radius at specified point.
            if self.mAlpha < self.mReductionPoint:
                if reductionFlag == False:
                    reductionFlag = True
                    reductionPoint = iterations
        print("Iterations: " + str(iterations) + "\n")
        # print("Neighborhood radius reduced after " + str(reductionPoint) + " iterations.\n")
        return
    def print_results(self, patternArray, vis):
        # Print clusters created.
        print("Clusters for training input:" + str(self.mMaxClusters))


        categories = []

        height, width = vis.shape[:2]


        resultVector = [0 for x in range(len(self.w))]
        for i in range(self.mNumPatterns):
            self.compute_input(patternArray, i)
            dMin = self.get_minimum(self.d)
            w = i % width

            # h = (i - w) / width
            h = i // width
            # print()
            # vis[h][w] = [i * 255 for i in FULL_WEIGHT_VECTOR[self.names[dMin]]]
            vis[h][w] = [i * 255 for i in self.w[dMin]]
            # print(self.w[dMin], dMin, h, w)
			# print("Vector (")
			# for j in range(self.mVectorLen):
			# 	print(j)
			# 	print(str(patternArray[i][j]) + ", ")


			# print(") fits into category " + name_of_vectors[dMin] + "\n")
            categories.append(dMin)

        for clust in categories:
            resultVector[clust] += 1.0
        max = 1;
        for i in range(0, len(resultVector)):
            resultVector[i] /= self.mNumPatterns


        res = str(resultVector).strip('[]')
        # name_of_file = self.picture.split('/');
        votes = []
        for i in range(0, len(resultVector)):
            print("Cluster " + self.names[i] + " contains: " + str(resultVector[i]))
            votes.append(resultVector[i])


        print("\n")
        # print(self.w)

        return [vis, votes]


def dataProcessing():
    clusters = []
    names = []

    centres = copy.deepcopy(FULL_WEIGHT_VECTOR)
    for name in centres:
        clusters.append(centres[name])
        names.append(name)

    while len(clusters) > 1:
        picture = cv2.imread('image.jpeg')
        height, width = picture.shape[:2]
        vis = np.zeros((height, width, 3), np.uint8)
        total_number = height * width
        picture = [i / 255 for i in picture]
        vectors = []
        for i in range(0, height):
            for j in range(0, width):
                vectors.append(picture[i][j])
        del picture


        kohonen = Kohonen(VEC_LEN, len(clusters), total_number, MIN_ALPHA, DECAY_RATE, RADIUS_REDUCTION_POINT, clusters, vectors, names)
        kohonen.training(vectors)
        result = kohonen.print_results(vectors, vis)
        vis = result[0]
        votes = result[1]
        cv2.imwrite("intermediate-results/intermediate-results"+ str(len(votes)) +".jpg", vis)
        minimum_value = min(votes)
        minimum_key = -1
        for i in range(len(votes)):
            if votes[i] == minimum_value:
                minimum_key = i
        if len(clusters) > 2:
            print(names[minimum_key] + ' is anomaly cluster. Deleting...\n')
        del names[minimum_key]
        clusters = []
        centres = copy.deepcopy(FULL_WEIGHT_VECTOR)
        # print(FULL_WEIGHT_VECTOR)
        # centres = FULL_WEIGHT_VECTOR
        for name in names:
            clusters.append(centres[name])

    return vis
with tf.device("/gpu:0"):
    dataProcessing()
